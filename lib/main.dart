import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:testci/screen/detail.dart';
import 'package:testci/screen/landing.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Landing(),
      title: "Landing Page",
      routes: {'/Detail':(context)=>Detail()},
    );
  }
}