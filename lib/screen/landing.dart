
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:testci/screen/home.dart';

class Landing extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Landing();
  }
}

class _Landing extends State<Landing>{


  int currentIndex = 0;
  void onItemTapped(int index) {
    setState(() {
      currentIndex = index;
      pageController.animateToPage(currentIndex, duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  void onPageChange(int index){
    setState(() {
      currentIndex = index;
    });
  }
  List<Widget> tablist =[
    Home(),
    Icon(Icons.directions_transit),
    Icon(Icons.directions_bike),
  ];

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final bottomtest = MediaQuery.of(context).viewInsets.bottom;

    return Scaffold(
      body: PageView(
        controller: pageController,
        onPageChanged: (index) {
          onPageChange(index);
        },
        children: tablist,
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: bottomtest),
        child: BottomAppBar(
          color: Colors.white,
          child: Container(
            color: Color(0xFFC9BC1F),
            height: MediaQuery.of(context).size.height * 0.07,
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width *0.5,
                  padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width *0.05, 0, 0, 0),
                  alignment: Alignment.centerLeft,
                  child: Icon(
                    Icons.update,
                    color:Colors.white
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width *0.5,
                  padding: EdgeInsets.fromLTRB(0, 0,MediaQuery.of(context).size.width *0.05, 0),
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.person_pin,
                    color:Colors.white
                  ),
                )
              ],
            ),
          ),
        ),
      ),
        floatingActionButton: FloatingActionButton(
              elevation: 0,
              onPressed: (){
                print("Add");
              },
              child: Container(
                  decoration: new BoxDecoration(
                    color: Color(0xFFFFEE58),
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.white,
                      width: 4,
                    ),
                  ),
                  width: MediaQuery.of(context).size.width *1,
                  height: MediaQuery.of(context).size.height *1,
                  child: Icon(Icons.add)
              ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked

    );
  }

}

