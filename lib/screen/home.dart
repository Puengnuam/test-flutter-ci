import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home>{

  TextEditingController giftvalue = TextEditingController();
  TextEditingController personvalue = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("จำนวนของขวัญ "),
                  Container(
                    width: 100,
                    child: TextFormField(
                      controller: giftvalue,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกจำนวนของขวัญ';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      style:  TextStyle(color: Colors.black),
                    ),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Text("จำนวนคน "),
                  Container(
                    width: 100,
                    child: TextFormField(
                      controller: personvalue,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'กรุณากรอกจำนวนคน';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.black),
                    ),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        Navigator.pushNamed(context, '/Detail',arguments: ({'gift':giftvalue.text,'person':personvalue.text}));
                      }
                    },
                    child: const Text('OK', style: TextStyle(fontSize: 20)),
                  ),
                ],
              )
            ],
          )
        ),
    );
  }

}